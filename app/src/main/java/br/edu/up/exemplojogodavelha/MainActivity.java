package br.edu.up.exemplojogodavelha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }


  char jogador = 'o';

  public void jogar(View v){
    ImageView btn = (ImageView) v;

    if (jogador == 'o') {
      btn.setImageResource(R.drawable.o_verde_hdpi);
      jogador = 'x';
    } else {
      btn.setImageResource(R.drawable.x_preto_hdpi);
      jogador = 'o';
    }
  }

}
